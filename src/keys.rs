// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::{io::Write, os::unix::fs::MetadataExt, path::Path, process::Command, fs::File, fs};

use xekeys::{ArrowKey, Keys};

use crate::{cursor::Cursor, file, render, terminal::Terminal, config::Conf};

pub fn use_keys(config2: Config, cursor2: Cursor, height: &usize, contents: usize, dir: &str, list: &str, conf: &Conf, current_dir: &str) -> Bulk {
    let mut config = config2;
    let mut cursor = cursor2;
    match xekeys::get_keys() {
        | Keys::CtrlChar('q') => config.kill = true,
        // Just for people who are confused
        | Keys::CtrlChar('z') => config.kill = true,
        // Toggle the bar
        | Keys::Char('b') => config.bar = !config.bar,
        | Keys::Char('B') => config.bar = !config.bar,
        | Keys::Char('.') => config.all_files = !config.all_files,
        | Keys::CtrlChar('t') => {
            let mut path = format!("{}/", current_dir);
            let term = Terminal::default();
            loop {
                match xekeys::get_keys() {
                    | Keys::Char('\n') => {
                        match File::create(path) {
                            | Ok(_) => (),
                            | Err(e) => {
                                // render status
                                print!("\x1b[107;30m\x1b[0;{}HCreate: {}\x1b[0m", term.get_size().1, e);
                                // print!() is buggy sometimes without refreshing...
                                std::io::stdout().flush().unwrap();
                                loop {
                                    match xekeys::get_keys() {
                                        | Keys::None => (),
                                        | _ => break
                                    }
                                }
                            }
                        }
                        break;
                    },
                    | Keys::Char(a) => path.push(a),
                    | Keys::Esc => {
                        break;
                    },
                    | Keys::Backspace => {
                        path.pop();
                    },
                    | _ => ()
                };
                // render status
                print!("\x1b[107;30m\x1b[{};0HCreate: {}{}\x1b[0m", term.get_size().1, path, " ".repeat(term.get_size().0 as usize - 9));
                // print!() is buggy sometimes without refreshing...
                std::io::stdout().flush().unwrap();
            }
        },
        | Keys::CtrlChar('d') => {
            let mut path = format!("{}/", current_dir);
            let term = Terminal::default();
            loop {
                match xekeys::get_keys() {
                    | Keys::Char('\n') => {
                        match fs::create_dir(path) {
                            | Ok(_) => (),
                            | Err(e) => {
                                // render status
                                print!("\x1b[107;30m\x1b[0;{}HCreate: {}\x1b[0m", term.get_size().1, e);
                                // print!() is buggy sometimes without refreshing...
                                std::io::stdout().flush().unwrap();
                                loop {
                                    match xekeys::get_keys() {
                                        | Keys::None => (),
                                        | _ => break
                                    }
                                }
                            }
                        }
                        break;
                    },
                    | Keys::Char(a) => path.push(a),
                    | Keys::Esc => {
                        break;
                    },
                    | Keys::Backspace => {
                        path.pop();
                    },
                    | _ => ()
                };
                // render status
                print!("\x1b[107;30m\x1b[{};0HCreate: {}{}\x1b[0m", term.get_size().1, path, " ".repeat(term.get_size().0 as usize - 9));
                // print!() is buggy sometimes without refreshing...
                std::io::stdout().flush().unwrap();
            }
        },
        | Keys::Arrow(ArrowKey::Right) | Keys::Char('\n') => {
            let list: Vec<&str> = list.split('\n').collect();
            let path = format!("{}/{}", dir, list[cursor.pos].replace('\r', ""));
            if Path::new(&path).is_dir() {
                crate::change_dir_to(path);
            } else {
                let file_type = list[cursor.pos].replace('\r', "");
                let file_type: Vec<&str> = file_type.split('.').collect();
                let file_type = file_type.last().unwrap();
                let mut cmd = "xdg-open";
                for i in &conf.file_conf {
                    if i.endings.contains(&file_type.to_string()) {
                        cmd = &i.cmd;
                    }
                }
                let mut child = match &file_type[..] {
                    // | "txt" | "rs" => Command::new(std::env::var("EDITOR").unwrap()).args(&[&path]).spawn().expect("failed"),
                    | _ => Command::new(cmd).args(&[&path]).spawn().expect("failed to execute process")
                };
                child.wait().unwrap();
                std::io::stdout().flush().unwrap();
            }
        },
        | Keys::Arrow(ArrowKey::Left) | Keys::Backspace => {
            let mut new_dir = "".to_owned();
            let dirs: Vec<&str> = dir.split('/').collect();
            if dirs.len() > 1 {
                for i in dirs.iter().take(dirs.len() - 1) {
                    new_dir = format!("{}/{}", new_dir, i);
                }
            }
            crate::change_dir_to(new_dir);
        },
        // Movement
        | Keys::Char('f') => cursor.move_to(0, height, contents),
        | Keys::Char('F') => cursor.move_to(usize::MAX, height, contents),
        | Keys::Arrow(ArrowKey::Up) => {
            cursor.move_by(-1);
            if cursor.offset == cursor.pos + 1 && cursor.offset != 0 {
                cursor.offset -= 1;
            }
        },
        | Keys::Arrow(ArrowKey::Down) => {
            cursor.move_by(1);
            if cursor.pos - cursor.offset - (height - 2) == 0 {
                cursor.offset += 1;
            }
        },
        | Keys::CtrlChar('f') => {
            // search
            let mut dirs: crate::Entry;
            let mut find = String::new();
            loop {
                match xekeys::get_keys() {
                    | Keys::Char('\n') => {
                        config.search = match find.is_empty() {
                            | true => None,
                            | false => Some(find)
                        };
                        break;
                    },
                    | Keys::Char(a) => find = format!("{}{}", find, a),
                    | Keys::Esc => {
                        break;
                    },
                    | Keys::Backspace => {
                        if !find.is_empty() {
                            find = find[0 .. find.len() - 1].to_owned();
                        };
                    },
                    | _ => ()
                };
                if config.kill {
                    config.kill = false;
                }
                if find.is_empty() {
                    dirs = crate::get_dir_contents(dir, config.all_files, None);
                } else {
                    dirs = crate::get_dir_contents(dir, config.all_files, Some(find.to_owned()));
                }
                let term = Terminal::default();
                render::render(dir, dirs.clone(), &term, config.bar, &cursor);
                let list: Vec<&str> = list.split('\n').collect();
                let info = list[cursor.pos].to_string().replace('\r', "");
                let owners = file::get_owners(&info, dir.to_owned());
                let metadata = file::get_metadata(&info, dir.to_owned());
                render::render_second_window(&info,
                                             (term.get_size().0 as usize, term.get_size().1 as usize),
                                             file::get_perms(&info, dir.to_owned()),
                                             file::get_file_size(&info, dir.to_owned()),
                                             config.bar,
                                             owners.0,
                                             owners.1,
                                             (file::get_time(metadata.atime() as u32), file::get_time(metadata.ctime() as u32), file::get_time(metadata.mtime() as u32)));
                // render status
                print!("\x1b[107;30m\x1b[{};0HFind: {}\x1b[0m", height, find);
                // print!() is buggy sometimes without refreshing...
                std::io::stdout().flush().unwrap();
            }
        },
        | _ => ()
    };
    Bulk { config, cursor }
}

#[derive(Clone)]
pub struct Config {
    pub bar:       bool,
    pub all_files: bool,
    // This isn't a config but it works best here.
    pub kill:      bool,
    pub search:    Option<String>
}

pub struct Bulk {
    pub config: Config,
    pub cursor: Cursor
}
