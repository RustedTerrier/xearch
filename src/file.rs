use std::{ffi::CStr, fs, os::unix::fs::{MetadataExt, PermissionsExt}, path::Path};

use libc::getpwuid;

pub fn get_owners(item: &str, current_dir: String) -> (&str, &str) {
    let path = format!("{}/{}", current_dir, item);
    let path = Path::new(&path);
    let metadata = fs::symlink_metadata(path).unwrap();
    let is_symlink = metadata.file_type().is_symlink();
    drop(metadata);
    match is_symlink {
        | true => {
            let metadata = fs::symlink_metadata(path).unwrap();
            (get_owner(&metadata), get_group(&metadata))
        },
        | false => {
            let metadata = fs::metadata(path).unwrap();
            (get_owner(&metadata), get_group(&metadata))
        }
    }
}

pub fn get_metadata(item: &str, current_dir: String) -> fs::Metadata {
    let path = format!("{}/{}", current_dir, item);
    let path = Path::new(&path);
    let metadata = fs::symlink_metadata(path).unwrap();
    let is_symlink = metadata.file_type().is_symlink();
    drop(metadata);
    match is_symlink {
        | true => fs::symlink_metadata(path).unwrap(),
        | false => fs::metadata(path).unwrap()
    }
}

pub fn get_file_size(item: &str, current_dir: String) -> u64 {
    let path = format!("{}/{}", current_dir, item);
    let path = Path::new(&path);
    let metadata = fs::symlink_metadata(path).unwrap();
    let is_symlink = metadata.file_type().is_symlink();
    drop(metadata);
    match is_symlink {
        | true => {
            let metadata = fs::symlink_metadata(path).unwrap();
            metadata.size()
        },
        | false => {
            let metadata = fs::metadata(path).unwrap();
            metadata.size()
        }
    }
}

pub fn get_time(epoch: u32) -> String {
    let mut epoch = epoch;
    epoch /= 60;
    // Remove 4 hours, not sure why exactly but the times don't match up otherwise.
    epoch -= 240;
    epoch /= 60;
    epoch /= 24;
    let year = epoch as f64 / 365.2425;
    let year = 1970 + year as u64;
    let leap_year = year % 4 == 0;
    let diy = 365.253;
    let mut month = match (epoch as f64 % diy) as u64 {
        | a @ 0 ..= 31 => ("1", a),
        | _ => {
            if leap_year {
                match (epoch as f64 % diy) as u64 {
                    | a @ 32 ..= 60 => ("2", a - 31),
                    | a @ 61 ..= 91 => ("3", a - 60),
                    | a @ 92 ..= 121 => ("4", a - 91),
                    | a @ 122 ..= 152 => ("5", a - 121),
                    | a @ 153 ..= 182 => ("6", a - 152),
                    | a @ 183 ..= 213 => ("7", a - 182),
                    | a @ 214 ..= 244 => ("8", a - 213),
                    | a @ 245 ..= 274 => ("9", a - 244),
                    | a @ 275 ..= 305 => ("10", a - 274),
                    | a @ 306 ..= 335 => ("11", a - 305),
                    | a => ("12", a - 335)
                }
            } else {
                match (epoch as f64 % diy) as u64 {
                    | a @ 32 ..= 59 => ("2", a - 31),
                    | a @ 60 ..= 90 => ("3", a - 60),
                    | a @ 91 ..= 120 => ("4", a - 91),
                    | a @ 121 ..= 151 => ("5", a - 121),
                    | a @ 152 ..= 181 => ("6", a - 152),
                    | a @ 182 ..= 212 => ("7", a - 182),
                    | a @ 213 ..= 243 => ("8", a - 213),
                    | a @ 244 ..= 273 => ("9", a - 244),
                    | a @ 274 ..= 304 => ("10", a - 274),
                    | a @ 305 ..= 334 => ("11", a - 305),
                    | a => ("12", a - 335)
                }
            }
        },
    };
    month.1 += 2;
    format!("{}-{}-{}", month.0, month.1, year)
}

pub fn get_perms(item: &str, current_dir: String) -> String {
    let path = format!("{}/{}", current_dir, item);
    let path = Path::new(&path);
    let perms = match path.is_dir() {
        | true => "d".to_owned(),
        | false => {
            let metadata = fs::symlink_metadata(path).unwrap();
            let file_type = metadata.file_type();
            match file_type.is_symlink() {
                | true => "l".to_owned(),
                | false => "-".to_owned()
            }
        }
    };

    let perm = match &perms[..] {
        | "l" => fs::symlink_metadata(path).unwrap().permissions().mode(),
        | _ => fs::metadata(path).unwrap().permissions().mode()
    };
    // Convert it to a &str and the perm value to octal.
    let perm: &str = &format!("{:o}", perm)[..];
    let file_type = &perm[0 .. perm.len() - 4];
    // NOTE: this will not display a directory as a symlink even if it is.
    let perms = match file_type {
        | "1" => "p".to_owned(),
        | "2" => "c".to_owned(),
        | "4" => "d".to_owned(),
        | "6" => "b".to_owned(),
        | "10" => "-".to_owned(),
        | "12" => "l".to_owned(),
        | "14" => "s".to_owned(),
        | _ => "-".to_owned()
    };
    // get the last 3 digits
    let perm_octal = &perm[perm.len() - 3 ..];
    let fourth_bit = &perm[perm.len() - 4 .. perm.len() - 3];
    format!("{}{}", perms, get_perms_from_octals(perm_octal, fourth_bit))
}

fn get_perms_from_octals(octal: &str, fourth: &str) -> String {
    let mut perms = get_perms_from_octal(&octal[0 .. 1], fourth, 1);
    perms = format!("{}{}", perms, get_perms_from_octal(&octal[1 .. 2], fourth, 2));
    perms = format!("{}{}", perms, get_perms_from_octal(&octal[2 .. 3], fourth, 3));
    perms
}

fn get_perms_from_octal(octal: &str, fourth_bit: &str, digit: u8) -> String {
    let perms = match octal {
        | "0" => "---",
        | "1" => "--x",
        | "2" => "-w-",
        | "3" => "-wx",
        | "4" => "r--",
        | "5" => "r-x",
        | "6" => "rw-",
        | "7" => "rwx",
        | _ => "---"
    };
    match fourth_bit {
        | "0" => perms.to_owned(),
        | "1" => {
            if digit == 3 {
                format!("{}{}", &perms[0 .. 2], match &perms[2 ..] {
                    | "x" => "t",
                    | _ => "T"
                })
            } else {
                perms.to_owned()
            }
        },
        | "2" => {
            if digit == 2 {
                format!("{}{}", &perms[0 .. 2], match &perms[2 ..] {
                    | "x" => "s",
                    | _ => "S"
                })
            } else {
                perms.to_owned()
            }
        },
        | "3" => {
            if digit == 3 {
                format!("{}{}", &perms[0 .. 2], match &perms[2 ..] {
                    | "x" => "t",
                    | _ => "T"
                })
            } else if digit == 2 {
                format!("{}{}", &perms[0 .. 2], match &perms[2 ..] {
                    | "x" => "s",
                    | _ => "S"
                })
            } else {
                perms.to_owned()
            }
        },
        | "4" => {
            if digit == 1 {
                format!("{}{}", &perms[0 .. 2], match &perms[2 ..] {
                    | "x" => "s",
                    | _ => "S"
                })
            } else {
                perms.to_owned()
            }
        },
        | "5" => {
            if digit == 3 {
                format!("{}{}", &perms[0 .. 2], match &perms[2 ..] {
                    | "x" => "t",
                    | _ => "T"
                })
            } else if digit == 1 {
                format!("{}{}", &perms[0 .. 2], match &perms[2 ..] {
                    | "x" => "s",
                    | _ => "S"
                })
            } else {
                perms.to_owned()
            }
        },
        | "6" => {
            if digit == 1 {
                format!("{}{}", &perms[0 .. 2], match &perms[2 ..] {
                    | "x" => "s",
                    | _ => "S"
                })
            } else if digit == 2 {
                format!("{}{}", &perms[0 .. 2], match &perms[2 ..] {
                    | "x" => "s",
                    | _ => "S"
                })
            } else {
                perms.to_owned()
            }
        },
        | "7" => {
            if digit == 3 {
                format!("{}{}", &perms[0 .. 2], match &perms[2 ..] {
                    | "x" => "t",
                    | _ => "T"
                })
            } else if digit == 1 {
                format!("{}{}", &perms[0 .. 2], match &perms[2 ..] {
                    | "x" => "s",
                    | _ => "S"
                })
            } else if digit == 2 {
                format!("{}{}", &perms[0 .. 2], match &perms[2 ..] {
                    | "x" => "s",
                    | _ => "S"
                })
            } else {
                perms.to_owned()
            }
        },
        | _ => perms.to_owned()
    }
}

pub fn binary_size(size: f64) -> String {
    const GIB: f64 = 1024.0 * 1024.0 * 1024.0;
    const MIB: f64 = 1024.0 * 1024.0;
    const KIB: f64 = 1024.0;
    if size >= GIB {
        format!("{} GiB", format_float(size / GIB))
    } else if size >= MIB {
        format!("{} MiB", format_float(size / MIB))
    } else if size >= KIB {
        format!("{} KiB", format_float(size / KIB))
    } else {
        format!("{} Bytes", size)
    }
}

pub fn decimal_size(size: f64) -> String {
    const GB: f64 = 1000.0 * 1000.0 * 1000.0;
    const MB: f64 = 1000.0 * 1000.0;
    const KB: f64 = 1000.0;
    if size >= GB {
        format!("{} GB", format_float(size / GB))
    } else if size >= MB {
        format!("{} MB", format_float(size / MB))
    } else if size >= KB {
        format!("{} KB", format_float(size / KB))
    } else {
        format!("{} Bytes", size)
    }
}

fn format_float(float: f64) -> String {
    let fstring = float.to_string();
    let mut vec: Vec<&str> = fstring.split('.').collect();
    if vec.len() > 1 {
        if vec[1].len() > 2 {
            vec[1] = &vec[1][0 .. 2];
        }
        format!("{}.{}", vec[0], vec[1])
    } else {
        vec[0].to_string()
    }
}

fn get_owner(metadata: &fs::Metadata) -> &'static str {
    let uid = metadata.uid();
    let uid_t = uid as libc::uid_t;
    let passwd = unsafe { getpwuid(uid_t) };
    let passwd = unsafe { passwd.as_ref() };
    let passwd = match passwd {
        | Some(a) => a,
        | None => std::process::exit(9)
    };
    let name = unsafe { CStr::from_ptr(passwd.pw_name) };
    name.to_str().unwrap()
}

fn get_group(metadata: &fs::Metadata) -> &'static str {
    let gid = metadata.gid();
    let gid_t = gid as libc::gid_t;
    let passwd = unsafe { getpwuid(gid_t) };
    let passwd = unsafe { passwd.as_ref() };
    let passwd = match passwd {
        | Some(a) => a,
        | None => std::process::exit(9)
    };
    let name = unsafe { CStr::from_ptr(passwd.pw_name) };
    name.to_str().unwrap()
}
