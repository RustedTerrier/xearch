// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#![allow(clippy::neg_multiply)]
pub struct Cursor {
    pub pos:    usize,
    pub offset: usize
}

impl Cursor {
    pub fn move_by(&mut self, increment: i32) {
        if increment < 0 {
            if (-1 * increment) as usize > self.pos {
                self.pos = 0;
            } else {
                self.pos += increment as usize;
            }
        } else {
            self.pos += increment as usize;
        }
    }

    pub fn move_to(&mut self, location: usize, height: &usize, contents: usize) {
        self.pos = location;
        if self.pos > contents {
            self.pos = contents;
        }
        if &self.pos > height {
            self.offset = self.pos - (height - 2);
        }
    }

    pub fn verify_location(&mut self, contents: usize, height: &usize) {
        if self.pos > contents {
            self.pos = contents;
        }
        if self.pos == 0 {
            self.offset = 0;
        }
        if self.offset > contents - (height - 3) {
            self.offset = contents - (height - 3);
        }
        if self.offset > self.pos {
            self.offset = self.pos - (height - 2)
        }
    }
}

impl Default for Cursor {
    fn default() -> Cursor {
        Cursor { pos: 0, offset: 0 }
    }
}
