// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::io::{stdout, Write};

use crate::{cursor, file, terminal};

pub fn get_window(height: usize, width: usize, title: &str, contents: String, highlight: Vec<String>, bar: bool, cursor: &cursor::Cursor) -> Vec<String> {
    let contents_vec: Vec<&str> = contents.split('\n').collect();
    let mut render_vec: Vec<String> = Vec::new();
    let half_of_bar = (width - title.len()) / 2;
    let mut window_title;
    if title.chars().count() <= width {
        window_title = format!("\x1b[107;30m{}{}{}", " ".repeat(half_of_bar), title, " ".repeat(half_of_bar));
    } else {
        window_title = format!("\x1b[107;30m{}", " ".repeat(width));
    }
    if half_of_bar * 2 + title.len() != width {
        window_title = format!("{} ", window_title);
    }
    render_vec.push(window_title);
    let bar = match bar {
        | true => "\x1b[107;30m ".to_owned(),
        | false => "\x1b[0m".to_owned()
    };
    if !highlight.is_empty() {
        let mut j = cursor.offset;
        for i in contents_vec.iter().skip(cursor.offset) {
            if j == cursor.pos {
                render_vec.push(format!("\r{}\x1b[0m \x1b[{}m\x1b[7m{}\x1b[0m", bar, highlight[j], &i));
            } else {
                render_vec.push(format!("\r{}\x1b[0m \x1b[{}m{}\x1b[0m", bar, highlight[j], &i));
            }
            j += 1;
        }
    } else {
        render_vec.push(format!("\r{}\x1b[0m Empty Directory.", bar));
    }
    while render_vec.len() + 1 < height {
        render_vec.push(format!("\r{}\x1b[0m", bar));
    }
    let window_bottom = format!("\r\x1b[107;30m{}\x1b[0m", " ".repeat(width));
    render_vec.push(window_bottom);
    while render_vec.len() > height as usize {
        render_vec.remove(render_vec.len() - 2);
    }
    render_vec
}

pub fn render(current_dir: &str, dir: crate::Entry, term: &terminal::Terminal, bar: bool, cursor: &cursor::Cursor) {
    // Alt screen because of a bug
    print!("\x1b[?7l\x1b[?1049h\x1b[?25l");
    // Clear the screen and then write everything
    print!("\x1b[2J\x1b[0m\x1b[92m");
    let render = crate::get_render(current_dir, dir, term, bar, cursor);
    print!("\x1b[H{}", render);
    stdout().flush().unwrap();
}

pub fn render_second_window(name: &str, win_size: (usize, usize), permissions: String, file_size: u64, bar: bool, owner: &str, group: &str, date: (String, String, String)) {
    let bar = match bar {
        | true => "\x1b[107;30m \x1b[0m".to_owned(),
        | false => "\x1b[0m".to_owned()
    };
    let spaces = " ".repeat((win_size.0 / 2) - 1);
    let width: usize = (win_size.0 / 2) + 1;
    let mut lines: Vec<String> = Vec::new();
    // Here we go to 1 past the middle width column of the second row.
    if !name.is_empty() {
        lines.push(format!("{} Name: {}{}", bar, name, spaces));
        lines.push(format!("{} Perm: {}{}", bar, permissions, spaces));
        lines.push(format!("{} {}", bar, spaces));
        lines.push(format!("{} Size: {} Bits{}", bar, file_size * 8, spaces));
        lines.push(format!("{} Deci: {}{}", bar, file::decimal_size(file_size as f64), spaces));
        lines.push(format!("{} Bina: {}{}", bar, file::binary_size(file_size as f64), spaces));
        lines.push(format!("{} {}", bar, spaces));
        lines.push(format!("{} Owne: {}{}", bar, owner, spaces));
        lines.push(format!("{} Grou: {}{}", bar, group, spaces));
        lines.push(format!("{} {}", bar, spaces));
        lines.push(format!("{} Acce: {}{}", bar, date.0, spaces));
        lines.push(format!("{} Chan: {}{}", bar, date.1, spaces));
        lines.push(format!("{} Modi: {}{}", bar, date.2, spaces));
    }
    // I do the window size - 2 because of the top and the bottom bars which we shouldnt render on
    // top of.
    match lines.len() < win_size.1 - 2 {
        | true => {
            while lines.len() < win_size.1 - 2 {
                lines.push(format!("{} {}", bar, spaces));
            }
        },
        | false => {
            if lines.len() > win_size.1 - 2 {
                while lines.len() > win_size.1 - 2 {
                    lines.remove(lines.len() - 1);
                }
            }
        },
    };
    for i in 1 ..= lines.len() {
        print!("\x1b[{};{}H{}\x1b[0m", i + 1, width, lines[i - 1]);
    }
    // print!() is buggy sometimes without refreshing...
    std::io::stdout().flush().unwrap();
}
