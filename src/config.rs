use std::env::var;

pub fn colors() -> Colors {
    let values = match var("XEARCH_COLOR") {
        | Ok(a) => a,
        | Err(_) => "32:31:33".to_owned()
    };
    let mut colors = Colors { dir: "11".to_owned(), symlink: "10".to_owned(), dirlink: "12".to_owned() };
    let values_vec: Vec<&str> = values.split(':').collect();
    if values_vec.len() <= 3 {
        colors.dir = values_vec[0].to_owned();
        if values_vec.len() > 1 {
            colors.symlink = values_vec[1].to_owned();
            if values_vec.len() > 2 {
                colors.dirlink = values_vec[2].to_owned();
            }
        }
    }
    colors
}

pub fn file_opening() -> Vec<FileConf> {
    match var("XEARCH_FILE_OPENING") {
        | Ok(val) => {
            let app: Vec<&str> = val.split("::").collect();
            let mut conf = Vec::new();
            for i in 0 .. app.len() {
                let endings: Vec<&str> = app[i].split(':').collect();
                let cmd = endings[0].to_owned();
                let mut endings_string: Vec<String> = Vec::new();
                for j in 1 .. endings.len() {
                    endings_string.push(endings[j].to_owned());
                }
                conf.push(FileConf {cmd, endings: endings_string});
            }
            conf
        },
        | Err(_) => Vec::new()
    }
}

pub struct Colors {
    pub dir:     String,
    pub symlink: String,
    pub dirlink: String
}

pub struct FileConf {
    pub cmd: String,
    pub endings: Vec<String>
}

pub struct Conf {
    pub colors:     Colors,
    pub change_dir: String,
    pub file_conf:  Vec<FileConf>
}

impl Default for Conf {
    fn default() -> Conf {
        match var("XEARCH_CD_ON_EXIT") {
            | Ok(change_dir) => Conf {colors: colors(), change_dir, file_conf: file_opening()},
            | Err(_) => Conf {colors: colors(), change_dir: String::new(), file_conf: file_opening()}
        }
    }
}
