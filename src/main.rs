// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::{env, fs, fs::read_dir, os::unix::fs::MetadataExt, path::Path};

mod config;
mod render;
mod window;
mod terminal;
mod cursor;
mod keys;
pub mod file;

fn main() {
    let conf = config::Conf::default();
    // Set up the cursor
    let mut cursor = cursor::Cursor::default();
    // Get the directory stuff
    let current_dir = start();
    let mut dir = get_dir_contents(&current_dir[..], false, None);

    let term = terminal::Terminal::default();
    // Render here and then rerender after a key is pressed
    render::render(&current_dir[..], dir.clone(), &term, true, &cursor);
    drop(current_dir);
    // Set up some stuff that can change during the run of the program.
    // I might make a struct for this later...
    let mut config = keys::Config { bar: true, all_files: false, kill: false, search: None };
    loop {
        let height = term.get_size().1 as usize;
        let start = start();
        dir = get_dir_contents(&start, config.all_files, config.clone().search);
        let dir_lines: Vec<&str> = dir.entry.split('\n').collect();
        // get kepresses
        let bulk = keys::use_keys(config, cursor, &height, dir_lines.len(), &start[..], &dir.entry[..], &conf, &start);
        cursor = bulk.cursor;
        config = bulk.config;
        if config.kill {
            break;
        }
        cursor.verify_location(dir_lines.len() - 1, &height);
        render::render(&start[..], dir.clone(), &term, config.bar, &cursor);
        let list: Vec<&str> = dir.entry.split('\n').collect();
        let info = list[cursor.pos].to_string().replace('\r', "");
        let owners = file::get_owners(&info, start.clone());
        let metadata = file::get_metadata(&info, start.clone());
        render::render_second_window(&info,
                                     (term.get_size().0 as usize, term.get_size().1 as usize),
                                     file::get_perms(&info, start.clone()),
                                     file::get_file_size(&info, start.clone()),
                                     config.bar,
                                     owners.0,
                                     owners.1,
                                     (file::get_time(metadata.atime() as u32), file::get_time(metadata.ctime() as u32), file::get_time(metadata.mtime() as u32)));
    }

    // Check if change on exit has a path and if so, write the current path to that file.
    if !conf.change_dir.is_empty() {
        match fs::write(conf.change_dir, start()) {
            Ok(_) => (),
            Err(_) => std::process::exit(4)
        };
    }
}

fn get_render(current_dir: &str, dir: Entry, term: &terminal::Terminal, bar: bool, cursor: &cursor::Cursor) -> String {
    let term_size = term.get_size();
    let window = window::Window { height: term_size.1 as usize, width: term_size.0 as usize, title: current_dir };
    let lines = render::get_window(window.height as usize, window.width as usize, window.title, dir.entry, dir.high_, bar, cursor);
    let mut render = lines[0].to_string();
    for i in lines.iter().skip(1) {
        render = format!("\r{}\n{}", render, i);
    }
    render
}

fn get_dir_contents(path: &str, all: bool, search: Option<String>) -> Entry {
    let entries = match read_dir(&path) {
        | Ok(contents) => contents,
        | Err(e) => {
            eprintln!("Error: {}", e);
            panic!("");
        }
    };
    let search = match &search {
        | Some(a) => a,
        | None => ""
    };
    let mut highlighters: Vec<String> = vec![];
    let mut contents_as_string = "".to_owned();
    let mut entries_vec: Vec<String> = Vec::new();
    for i in entries {
        // Format the entry
        entries_vec.push(i.unwrap().path().display().to_string());
    }
    entries_vec.sort();
    let path = format!("{}/", path);
    for i in entries_vec {
        if i.replace(&path, "").to_ascii_uppercase().contains(&search.to_ascii_uppercase()) {
            if all {
                if contents_as_string.is_empty() {
                    contents_as_string = i.clone().replace(&path, "");
                } else {
                    contents_as_string = format!("{}\r\n{}", contents_as_string, &i.replace(&path, ""));
                }
                highlighters.push(get_file_type(i, config::colors()));
            } else if &i.replace(&path, "")[0 .. 1] != "." {
                if contents_as_string.is_empty() {
                    contents_as_string = i.clone().replace(&path, "");
                } else {
                    contents_as_string = format!("{}\r\n{}", contents_as_string, &i.replace(&path, ""));
                }
                highlighters.push(get_file_type(i, config::colors()));
            }
        }
    }
    Entry { entry: contents_as_string, high_: highlighters }
}

fn get_file_type(path: String, colors: config::Colors) -> String {
    if Path::new(&path).is_dir() {
        if Path::new(&path).canonicalize().unwrap() != Path::new(&path) {
            colors.dirlink
        } else {
            colors.dir
        }
    } else {
        let metadata = fs::symlink_metadata(path).unwrap();
        let file_type = metadata.file_type();
        match file_type.is_symlink() {
            | true => colors.symlink,
            | false => "0".to_string()
        }
    }
}

fn start() -> String {
    let current_dir = match env::current_dir() {
        | Ok(dir) => dir.display().to_string(),
        | Err(e) => {
            eprintln!("Error: {}", e);
            panic!("");
        }
    };
    current_dir
}

fn change_dir_to(path: String) {
    match env::set_current_dir(path) {
        | Ok(()) => (),
        | Err(e) => panic!("Could not change directory: {}", e)
    };
}

#[derive(Clone)]
pub struct Entry {
    pub entry: String,
    pub high_: Vec<String>
}
