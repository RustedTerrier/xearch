# XEARCH

Xearch aims to be a configurable file manager written with a focus on speed.

### State of the project:

##### Xearch is still a work in progress.

![image alt text](xearch_screenshot.png "current_xearch_ex")

### Known Bugs:
 - ~~Crashing in an empty directory~~
 - Crashing in a directory the user doesn't have access to.
