# Features:

 - [X] XDG-Open
 - [X] Dual-pane
 - [X] Movement
   - [ ] Configurable movement
 - [X] Colors
   - [X] Configurable colors
 - [X] Searching
 - [X] Configurable file opening
 - [ ] Moving/Renaming files
 - [ ] Removing files
 - [ ] Changing user
 - [ ] Changing permissions
 - [X] Creating files
 - [X] Creating directories
 - [ ] Documentation
